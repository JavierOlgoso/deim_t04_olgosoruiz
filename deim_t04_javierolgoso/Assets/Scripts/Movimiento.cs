using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour
{
    public Rigidbody2D rb;
    private Animator animator;
    public float fuerzasalto;
    public float velocidad;
    public LayerMask Tierra;
    public float DistanciaRay;
    
    
    public bool sueloesta;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 1;
        RaycastHit2D hit2D= Physics2D.Raycast(position, direction, distance, Tierra);
        if (hit2D.collider)
        {
            sueloesta = true;
        }
        else
        {
            sueloesta = false;
        }
        bool Ground()
        {
            Vector2 position = transform.position;
            Vector2 direction = Vector2.down;
            float distance = 200;
            RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, Tierra);

            hit.collider.gameObject.SendMessage("arycast");
            if (hit.collider != null)
            {
                return true;
            }
            return false;

        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(Vector3.left * velocidad);
            animator.SetBool("Correr", true);
            GetComponent<SpriteRenderer>().flipX = true;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            animator.SetBool("Correr", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(Vector3.right * velocidad);
            animator.SetBool("Correr", true);
            GetComponent<SpriteRenderer>().flipX = false;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("Correr", false);
        }
        if (Input.GetKeyUp(KeyCode.Space)&&(sueloesta==true))
        {

            rb.AddForce(Vector2.up * fuerzasalto, ForceMode2D.Impulse);
            if (rb.velocity.y != 0)
            {
                animator.SetBool("Saltar", true);
            }

        }
        if (rb.velocity.y == 0)
        {
            animator.SetBool("Saltar", false);
        }

        if (rb.velocity.y < 0)
        {
            animator.SetBool("Caer", true);
        }
        else if (rb.velocity.y > -2500)
        {
            animator.SetBool("Caer", false);
        }

       


        









    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            GameManager.death = true;
        }
    }

}
    
        
    
    
        
    
    
