using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public static int score;
    public static bool death;
    public GameObject GameOver;

    private void Awake()
    {
        GameOver.SetActive(false);
        death = false;
    }
    void Start()
    {
        GameOver.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        if (death == true)
        {
            death1();
        }
    }
  public void death1()
    {
        GameOver.SetActive(true);
    }
    public void Button()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
}
