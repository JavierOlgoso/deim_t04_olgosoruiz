using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    public Animator animatore;
    public GameObject moneda;
    public int contador;
    // Start is called before the first frame update
    void Start()
    {
        animatore = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
  
  
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            
            animatore.SetBool("Abrir", true);
            for (int i = 1; i <= contador; i++)
            {
                Instantiate<GameObject>(moneda, transform.position, Quaternion.identity);
            }
            

        }
        
        
    
    }
  
}
