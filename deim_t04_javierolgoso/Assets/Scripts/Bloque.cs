using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloque : MonoBehaviour
{
    public Animator animator;
    public GameObject jugador;
    public LayerMask Player;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 100;
        RaycastHit2D hit2D = Physics2D.Raycast(position, direction, distance,Player);
        if (hit2D.collider)
        {
            animator.SetBool("Contacto" , true);
            StartCoroutine(UsingYield(1));
      
        }
       
    }
    IEnumerator UsingYield(int seconds)
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("Contacto", false);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(jugador);
            GameManager.death = true;
        }
    }
}
