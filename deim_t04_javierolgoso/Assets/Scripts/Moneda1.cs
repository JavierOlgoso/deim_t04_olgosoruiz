using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda1 : MonoBehaviour
{
    public Transform cofreee;
    public Rigidbody2D rb;
    // Start is called before the first frame update
    private void Awake()
    {
        gameObject.transform.position = cofreee.position + new Vector3(0, 1, 0);
        rb.velocity = new Vector3(rb.velocity.x, Random.Range(-3, 3));
        rb.velocity = new Vector3(rb.velocity.y, 5);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.score++;
            Destroy(gameObject);
        }
    }
}
